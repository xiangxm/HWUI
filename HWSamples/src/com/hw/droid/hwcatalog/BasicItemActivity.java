package com.hw.droid.hwcatalog;

import hwdroid.app.HWListActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.DrawableText2Item;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.Item.Type;
import hwdroid.widget.item.SeparatorItem;
import hwdroid.widget.item.Text2Item;
import hwdroid.widget.itemview.ItemView;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class BasicItemActivity extends HWListActivity {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.showBackKey(true);
        
        List<Item> items = new ArrayList<Item>();
        
        items.add(new SeparatorItem("drawable item"));
        
        items.add(new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789", this.getResources().getDrawable(R.drawable.aui2_5), this.getResources().getDrawable(R.drawable.aui2_5)));
        items.add(new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789"));
        items.add(new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789"));
        items.add(new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "", this.getResources().getDrawable(R.drawable.aui2_5)));

        items.add(new SeparatorItem("drawable item (select mode)"));
        
        DrawableText2Item item1 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", this.getResources().getDrawable(R.drawable.aui2_5));
        item1.setTypeMode(Type.CHECK_MODE);
        DrawableText2Item item2 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789");
        item2.setTypeMode(Type.CHECK_MODE);
        DrawableText2Item item3 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789" );
        item3.setTypeMode(Type.RADIO_MODE);
        DrawableText2Item item4 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "", this.getResources().getDrawable(R.drawable.aui2_5));
        item4.setTypeMode(Type.RADIO_MODE);
        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);   
        
        items.add(new SeparatorItem("drawable item (unenable mode)"));

        DrawableText2Item item5 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789", this.getResources().getDrawable(R.drawable.aui2_5));
        item5.setTypeMode(Type.CHECK_MODE);
        item5.setEnabled(false);
        DrawableText2Item item6 = new DrawableText2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789");
        item6.setTypeMode(Type.CHECK_MODE);
        item6.setEnabled(false);
        items.add(item5);
        items.add(item6);
        
        items.add(new SeparatorItem("subtitle item"));
        
        items.add(new Text2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789"));
        items.add(new Text2Item("abcdefghijklmnopqrstuvwxyz0123456789"));
        items.add(new Text2Item("abcdefghijklmnopqrstuvwxyz0123456789", ""));  

        items.add(new SeparatorItem("subtitle item (check mode)"));
        
        Text2Item item7 = new Text2Item("abcdefghijklmnopqrstuvwxyz0123456789", "abcdefghijklmnopqrstuvwxyz0123456789");
        item7.setTypeMode(Type.CHECK_MODE);
        Text2Item item8 = new Text2Item("abcdefghijklmnopqrstuvwxyz0123456789" );
        item8.setTypeMode(Type.CHECK_MODE);
        items.add(item7);
        items.add(item8);
        
        final ItemAdapter adapter = new ItemAdapter(this, items);
        adapter.setSubTextSingleLine(false);
        setListAdapter(adapter);
    }
    
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	ItemView view = (ItemView) v;
    	
    	ItemAdapter adapter = (ItemAdapter)l.getAdapter();
    	Item item = (Item)adapter.getItem(position);
    	item.setChecked(!item.isChecked());
    	view.setObject(item);
    }
}
