package com.hw.droid.hwcatalog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.hw.droid.hwcatalog.indicator.FlexibleTabIndicatorActivity;
import com.hw.droid.hwcatalog.indicator.SimpleTabIndicatorActivity;
import com.hw.droid.hwcatalog.indicator.TabIndicatorActivity;
import com.hw.droid.hwcatalog.preference.PreferenceSample;

import hwdroid.app.HWActivityInterface;
import hwdroid.app.HWListActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.TextItem;

public class CatalogActivity extends HWListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSwipeBackEnable(false);
        this.setTitle2("AUI2.5");
        ItemAdapter adapter = new ItemAdapter(this);
        adapter.add(createTextItem("1.List items Layout(1)", BasicItemActivity.class));
        adapter.add(createTextItem("3.List items Layout(3)", SelectListViewActivity.class));
        adapter.add(createTextItem("4.List items Layout(Widget mode)", WidgetItemActivity.class));
        adapter.add(createTextItem("5.Sliding List item", SlidingLeftItemListViewActivity.class));        
        adapter.add(createTextItem("6.ItemAdapter", ItemAdapterSample.class));
        adapter.add(createTextItem("7.ItemCursorAdapter", ItemCursorAdapterSamples.class));
        adapter.add(createTextItem("8.ActionBar", ActionBarActivity.class));
        adapter.add(createTextItem("9.FooterBarMenu", FooterBarMenuActivity.class));
        adapter.add(createTextItem("10.FooterBarButton", FooterBarButtonActivity.class));
        adapter.add(createTextItem("11.Common Control", CommonControl.class));
        adapter.add(createTextItem("12.Expandable List View", ExpandableList.class));        
        adapter.add(createTextItem("13.ListPopupWindow", ListPopupWindowActivity.class));        
        adapter.add(createTextItem("14.Dialog(aui2.5)", DropDownDialogActivity.class));
        adapter.add(createTextItem("15.Dialog(aui3.0)", AlertDialogSamples.class));
        adapter.add(createTextItem("16.Preference", PreferenceSample.class));
        adapter.add(createTextItem("17.Flexible Tab Indicator", FlexibleTabIndicatorActivity.class));
        adapter.add(createTextItem("18.Tab Indicator", TabIndicatorActivity.class));
        adapter.add(createTextItem("19.Simple Tab Indicator", SimpleTabIndicatorActivity.class));
        adapter.add(createTextItem("20.Search View", SearchViewActivity.class));
        adapter.add(createTextItem("21.Search View 2", SearchView2Activity.class));
        adapter.add(createTextItem("22.Search View 3", SearchView3Activity.class));
        setListAdapter(adapter);
    }
    
    private TextItem createTextItem(String str, Class<?> klass) {
        final TextItem textItem = new TextItem(str);
        textItem.setTag(klass);
        return textItem;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final TextItem textItem = (TextItem) l.getAdapter().getItem(position);
        Intent intent = new Intent(CatalogActivity.this, (Class<?>) textItem.getTag());
        intent.putExtra(HWActivityInterface.HW_ACTION_BAR_TITLE, ((TextView)v).getText());
        startActivity(intent);
    	
    	
    }
}
