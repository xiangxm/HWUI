package com.hw.droid.hwcatalog;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import hwdroid.app.HWListActivity;
import hwdroid.dialog.AlertDialog;
import hwdroid.dialog.Dialog;
import hwdroid.dialog.DialogInterface;
import hwdroid.dialog.DialogInterface.OnCancelListener;
import hwdroid.dialog.ProgressDialog;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.TextItem;

public class DropDownDialogActivity extends HWListActivity{
	
	Toast mToast;
	ProgressDialog mProgressDialog;
	int mProgressValue;
	
	Handler mHandler = new Handler();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.showBackKey(true);
        mToast = new Toast(this); 
        
        ItemAdapter adapter = new ItemAdapter(this);
        adapter.add(createTextItem("message style"));
        adapter.add(createTextItem("items style"));
        adapter.add(createTextItem("single choice items style"));
        adapter.add(createTextItem("multi choice items style"));
        adapter.add(createTextItem("progress bar style"));
        adapter.add(createTextItem("custom view style"));
        
        setListAdapter(adapter);     
    }
    
    private TextItem createTextItem(String str) {
        final TextItem textItem = new TextItem(str);
        return textItem;
    }
    
    private void showToast(CharSequence text) {
    	TextView t = new TextView(this);
    	t.setBackgroundColor(Color.RED);
    	t.setText(text);
    	t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if( position == 0) {
            AlertDialog alertDialog = new AlertDialog.Builder(this). 
	                setTitle("title"). 
	                //setMessage("message").
	                //setCancelable(true).
	                setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	                    
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                    	showToast("ok");
	                    } 
	                }).
	                create(); 
            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener(){

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        showToast("press back key!!");
                    }
                    
                    return false;
                }});
            
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                
                @Override
                public void onCancel(DialogInterface dialog) {
                    showToast("cancel listener!!");
                }
            });
            
            
	        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {
				    showToast("dismiss listener!!");
					//dialog.dismiss();
				}
			});
	        alertDialog.show();
	        
        } else if(position == 1) {
        	
        	CharSequence[] items = new CharSequence[]{"item1", "item2", "item3"};
	        Dialog alertDialog = new AlertDialog.Builder(this). 
	                setTitle("title"). 
	                setCancelable(true).
	                setItems(items, new DialogInterface.OnClickListener(){

						@Override
						public void onClick(DialogInterface dialog, int which) {
							showToast("chick item " + (which));
							//dialog.cancel();
							
						}}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { 
		                     
		                    @Override 
		                    public void onClick(DialogInterface dialog, int which) { 
		                    	showToast("cancel");
		                    } 
		                }).create(); 
	        alertDialog.show();        	
        } else if(position == 2) {
        	
        	CharSequence[] items = new CharSequence[]{"item1", "item2", "item3"};
	        Dialog alertDialog = new AlertDialog.Builder(this). 
	                setTitle("title").
	                setCancelable(true).
	                setSingleChoiceItems(items, 1,  new DialogInterface.OnClickListener(){

						@Override
						public void onClick(DialogInterface dialog, int which) {
							showToast("chick item " + (which));
							//dialog.cancel();
							
						}}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { 
		                     
		                    @Override 
		                    public void onClick(DialogInterface dialog, int which) { 
		                    	showToast("cancel");
		                    } 
		                }).create(); 
	        alertDialog.show();        	
        }
        else if(position == 3) {
        	boolean[] choiceStatus = new boolean[]{false, true, true};
        	CharSequence[] items = new CharSequence[]{"item1", "item2", "item3"};
	        Dialog alertDialog = new AlertDialog.Builder(this). 
	                setTitle("title").
	                setCancelable(true).
	                setMultiChoiceItems(items, choiceStatus,  new DialogInterface.OnMultiChoiceClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which,
								boolean isChecked) {
							showToast("item " + which  + " is " + (isChecked?"choice":"not choice"));
						}
					}).setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	                    
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                    	showToast("ok");
	                    } 
	                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { 
		                     
		                    @Override 
		                    public void onClick(DialogInterface dialog, int which) { 
		                    	showToast("cancel");
		                    } 
		                }).create(); 
	        alertDialog.show();        	
        } else if (position == 4) {
            ProgressDialog dialog = new ProgressDialog(this);
            mProgressDialog = dialog;

            dialog.setCancelable(true);
            dialog.setMessage("正在搜索中...");
            dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "取消", null);
            dialog.setMax(100);
            dialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    showToast("cancel");
                }
            });
            dialog.show();

            mProgressValue = 0;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (mProgressValue++ < 100) {
                        mProgressDialog.incrementProgressBy(1);
                        SystemClock.sleep(100);
                    }
                }
            }).start();
        }else if(position == 5) {     	
            LinearLayout layout = new LinearLayout(DropDownDialogActivity.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, Gravity.CENTER);
            layout.setLayoutParams(lp);
            
            LinearLayout layout1 = new LinearLayout(DropDownDialogActivity.this);
            //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, Gravity.CENTER);
            layout1.setLayoutParams(lp);
            
            layout.addView(layout1);
            
            Button btn = new Button(DropDownDialogActivity.this);
            

            

            
            btn.setText("custom view");
            
            layout1.addView(btn);
        	
        	Dialog alertDialog = new AlertDialog.Builder(this). 
	                setTitle("title").
	                setCancelable(true).
	                setView(layout).
	                setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	                    
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                    	showToast("ok");
	                    } 
	                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { 
		                     
		                    @Override 
		                    public void onClick(DialogInterface dialog, int which) { 
		                    	showToast("cancel");
		                    } 
		                }).create(); 
	        alertDialog.show();        	
        }
    }
}
